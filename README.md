# Starter kit SF4 et docker

## Prérequis
> Docker
> Docker-compose

## Container
> nginx 1.16.*

> php-fpm 7.3.*
>> composer

>>yarn et node.js

>mysql 8.*

## Installation
### Démarrer docker et se connecter
>sudo docker-compose up --build

>sudo docker-compose exec php sh
>> yarn

yarn permet de lancer l'installation de tous les composants nécessaires au fonctionnement de webpack.
Il récupère également la librairie materialize.

### URL
> http://localhost:8002/

### Symfony
Configuration par défaut
````
        "sensio/framework-extra-bundle": "^5.1",
        "symfony/asset": "4.3.*",
        "symfony/console": "4.3.*",
        "symfony/dotenv": "4.3.*",
        "symfony/expression-language": "4.3.*",
        "symfony/flex": "^1.3.1",
        "symfony/form": "4.3.*",
        "symfony/framework-bundle": "4.3.*",
        "symfony/http-client": "4.3.*",
        "symfony/intl": "4.3.*",
        "symfony/monolog-bundle": "^3.1",
        "symfony/orm-pack": "*",
        "symfony/process": "4.3.*",
        "symfony/security-bundle": "4.3.*",
        "symfony/serializer-pack": "*",
        "symfony/swiftmailer-bundle": "^3.1",
        "symfony/templating": "4.3.*",
        "symfony/translation": "4.3.*",
        "symfony/twig-bundle": "4.3.*",
        "symfony/validator": "4.3.*",
        "symfony/web-link": "4.3.*",
        "symfony/webpack-encore-bundle": "^1.7",
        "symfony/yaml": "4.3.*"
````
